/*
 * system_trajectory.hpp
 *
 *  Created on: Jun 1, 2015
 *      Author: depardo
 */

#ifndef SYSTEM_TRAJECTORY_HPP_
#define SYSTEM_TRAJECTORY_HPP_


#include <stdlib.h>
#include <iostream>
#include <memory>
#include <spline_module/spline.hpp>
#include <Eigen/Dense>
#include <dynamical_systems/base/DynamicsBase.hpp>

template<class DIMENSIONS>
class systemTrajectorySpline {

public:
	// Required as class has Eigen Members
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::state_vector_array_t state_vector_array_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;
	typedef typename DIMENSIONS::control_vector_array_t control_vector_array_t;

	systemTrajectorySpline( std::shared_ptr<DynamicsBase<DIMENSIONS> >dynamics,
			const state_vector_array_t & x_trajectory , const control_vector_array_t & u_trajectory, const double & dt , bool hermite , bool verbose = false);

	systemTrajectorySpline( std::shared_ptr<DynamicsBase<DIMENSIONS> >system_dynamics,
			const state_vector_array_t & state_trajectory, const control_vector_array_t & control_trajectory, const Eigen::VectorXd & time_vector,
			bool hermite_type, bool verbose = false);

	void getTrajectoryPoint(const double &t , state_vector_t & x_t, control_vector_t & u_t , state_vector_t & dxdt_t);

private:

	void getSingleStateSequence(const int & state_id , Eigen::VectorXd & single_state_sequence, Eigen::VectorXd & single_state_timederivative_sequence);
	void getSingleControlSequence(const int & control_id , Eigen::VectorXd & single_control_sequence);

	// pointer to system dynamics
	std::shared_ptr<DynamicsBase<DIMENSIONS> > system_dynamics;

	// a spline for each state
	std::vector<std::shared_ptr<double> > state_spline;

	state_vector_array_t state_trajectory;
	control_vector_array_t control_trajectory;
	state_vector_array_t state_timederivatives_trajectory;
	std::vector<double> time_trajectory;

	double deltaT;
	int num_states;
	int num_controls;
	int trajectory_size;
	double final_time;
	bool hermite_type;
};

template <class DIMENSIONS>
systemTrajectorySpline<DIMENSIONS>::systemTrajectorySpline(std::shared_ptr<DynamicsBase<DIMENSIONS> > dynamics ,
														   const state_vector_array_t & x_trajectory , const control_vector_array_t & u_trajectory, const double & dt , bool hermite ,bool verbose /*= true */)
														   :system_dynamics(dynamics) , state_trajectory(x_trajectory) , control_trajectory(u_trajectory), deltaT(dt),
														    hermite_type(hermite) {

	if(deltaT < 1e-5) {
		std::cout << "ERROR : Time increment should be bigger than 1e-5" << std::endl;
		std::exit(EXIT_FAILURE);
	}

	/* ToDo : Read this from DIMENSIONS */
	num_states = state_trajectory[0].rows();;
	num_controls = control_trajectory[0].rows();
	trajectory_size = state_trajectory.size();

	//ToDo : Validate that control_trajectory has the same size

	time_trajectory.push_back(0.0); // time[0] = 0.0

	for(int i = 1 ; i < trajectory_size ; i++)	{

		// create time trajectory
		time_trajectory.push_back(time_trajectory[i-1] + deltaT);

		// system dynamics

		state_vector_t xdot = system_dynamics->systemDynamics(state_trajectory[i] ,control_trajectory[i]);

		state_timederivatives_trajectory.push_back(xdot);
	}

	final_time = time_trajectory[trajectory_size-1];

	if(hermite_type) {

		for(int state_nbr = 0 ; state_nbr < num_states ; state_nbr ++) {

			Eigen::VectorXd single_state_sequence;

			Eigen::VectorXd single_state_timederivative_sequence;

			getSingleStateSequence(state_nbr , single_state_sequence , single_state_timederivative_sequence);

			std::shared_ptr<double> single_state_spline(spline_hermite_set( trajectory_size, time_trajectory.data(), single_state_sequence.data() , single_state_timederivative_sequence.data() ) );

			state_spline.push_back(single_state_spline);
		}
	}

	if(verbose)	{

		std::cout << "---" << std::endl << "creating Trajectory Spline " << std::endl;
		std::cout << "hermite type : " << hermite_type << std::endl;
		std::cout << "num_states : " << num_states << std::endl;
		std::cout << "num_controls : " << num_controls << std::endl;
		std::cout << "deltaT : " << deltaT << std::endl;
		std::cout << "trajectory_size : " << trajectory_size << std::endl;
		std::cout << "Trajectories are ready " << std::endl;
		std::cout << "final_time : " << final_time << std::endl;
		std::cout << "---" << std::endl;
	}
}

template <class DIMENSIONS>
systemTrajectorySpline<DIMENSIONS>::systemTrajectorySpline( std::shared_ptr<DynamicsBase<DIMENSIONS> >system_dynamics,
			const state_vector_array_t & state_trajectory, const control_vector_array_t & control_trajectory, const Eigen::VectorXd & time_vector,
			bool hermite_type, bool verbose):system_dynamics(system_dynamics) , state_trajectory(state_trajectory) , control_trajectory(control_trajectory), deltaT(0.0),
		    hermite_type(hermite_type) {


	num_states = static_cast<int>(DIMENSIONS::DimensionsSize::STATE_SIZE); //state_trajectory[0].rows();;
	num_controls = static_cast<int>(DIMENSIONS::DimensionsSize::CONTROL_SIZE); //control_trajectory[0].rows();

	// reset all vectors
	state_timederivatives_trajectory.clear();
	time_trajectory.clear();
	state_spline.clear();
	trajectory_size = state_trajectory.size();

	for(int i = 0 ; i < trajectory_size ; i++) {

		// system dynamics and time trajectories

		state_vector_t xdot = system_dynamics->systemDynamics(state_trajectory[i] ,control_trajectory[i]);
		state_timederivatives_trajectory.push_back(xdot);
		time_trajectory.push_back(time_vector(i));
	}

	final_time = time_trajectory[trajectory_size-1];

	// prepare stuff for HermiteSpline
	if(hermite_type) {

		for(int state_nbr = 0 ; state_nbr < num_states ; state_nbr ++) {

			Eigen::VectorXd single_state_sequence;
			Eigen::VectorXd single_state_timederivative_sequence;

			getSingleStateSequence(state_nbr , single_state_sequence , single_state_timederivative_sequence);

			std::shared_ptr<double> single_state_spline(spline_hermite_set( trajectory_size, time_trajectory.data(), single_state_sequence.data() , single_state_timederivative_sequence.data() ) );

			state_spline.push_back(single_state_spline);
		}
	}


	if(verbose) {
		std::cout << "---" << std::endl << "creating Trajectory Spline " << std::endl;

		std::cout << "hermite type : " << hermite_type << std::endl;
		std::cout << "num_states : " << num_states << std::endl;
		std::cout << "num_controls : " << num_controls << std::endl;
		std::cout << "trajectory_size : " << trajectory_size << std::endl;
		std::cout << "Trajectories are ready " << std::endl;
		std::cout << "final_time : " << final_time << std::endl;
		std::cout << "---" << std::endl;
	}
}

template <class DIMENSIONS>
void systemTrajectorySpline<DIMENSIONS>::getSingleStateSequence(const int & state_id , Eigen::VectorXd & single_state_sequence,
		Eigen::VectorXd & single_state_timederivative_sequence) {

	single_state_sequence.resize(trajectory_size);

	single_state_timederivative_sequence.resize(trajectory_size);

	for(int i = 0 ; i < trajectory_size ; i++) {

		//std::cout << "i:" << i << std::endl;
		single_state_sequence[i] = state_trajectory[i](state_id);
		single_state_timederivative_sequence[i] = state_timederivatives_trajectory[i](state_id);
	}
}

template <class DIMENSIONS>
void systemTrajectorySpline<DIMENSIONS>::getSingleControlSequence(const int & control_id , Eigen::VectorXd & single_control_sequence) {

	single_control_sequence.resize(trajectory_size);

	for(int i = 0 ; i < trajectory_size ; i++) {

		single_control_sequence[i] = control_trajectory[i](control_id);
	}
}

template <class DIMENSIONS>
void systemTrajectorySpline<DIMENSIONS>::getTrajectoryPoint(const double & t , state_vector_t & x_t, control_vector_t & u_t,
		state_vector_t & dxdt_t) {

	for(int state_nbr = 0 ; state_nbr < num_states ; state_nbr++) {

		if(hermite_type) {

			spline_hermite_val( trajectory_size , time_trajectory.data() ,  state_spline[state_nbr].get()  , t , &x_t(state_nbr) ,  &dxdt_t(state_nbr));
		} else {

			Eigen::VectorXd single_state_sequence;
			Eigen::VectorXd single_state_derivative_sequence;
			getSingleStateSequence(state_nbr , single_state_sequence, single_state_derivative_sequence );
			double slope; /* useless, but required */
			spline_linear_val( trajectory_size , time_trajectory.data() , single_state_sequence.data() , t , &x_t(state_nbr) ,  &slope);
		}
	}

	for(int control_nbr = 0 ; control_nbr < num_controls ; control_nbr ++) {

		Eigen::VectorXd single_control_sequence;
		getSingleControlSequence(control_nbr , single_control_sequence );
		double slope; /* useless, but required */
		spline_linear_val( trajectory_size , time_trajectory.data() , single_control_sequence.data() , t , &u_t(control_nbr) ,  &slope);
	}

}

#endif /* SYSTEM_TRAJECTORY_HPP_ */
